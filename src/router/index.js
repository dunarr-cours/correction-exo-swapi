import Vue from 'vue'
import VueRouter from 'vue-router'
import FilmIndex from '@/views/FilmIndex.vue';
import FilmShow from '@/views/FilmShow.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'film_index',
    component: FilmIndex
  },
  {
    path: '/:id',
    name: 'film_show',
    component: FilmShow
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
